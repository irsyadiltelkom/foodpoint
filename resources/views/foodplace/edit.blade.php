@extends('layouts.master')

@section('title','Edit Food Place')

@section('content')
<h4>Edit Food Place</h4>
<form action="{{ route('foodplace.update', $foodplace->id) }}" method="post">
    {{csrf_field()}}
    {{ method_field('PUT') }}
    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
        <label for="name" class="control-label">Name</label>
        <input type="text" class="form-control" name="name" placeholder="Name" value="{{ $foodplace->name }}">
        @if ($errors->has('name'))
            <span class="help-block">{{ $errors->first('name') }}</span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('address') ? 'has-error' : '' }}">
        <label for="address" class="control-label">Address</label>
        <textarea name="address" cols="30" rows="5" class="form-control">{{ $foodplace->address }}</textarea>
        @if ($errors->has('address'))
            <span class="help-block">{{ $errors->first('address') }}</span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('latitude') ? 'has-error' : '' }}">
        <label for="latitude" class="control-label">Latitude</label>
        <input type="text" class="form-control" name="latitude" placeholder="Latitude" value="{{ $foodplace->latitude }}">
        @if ($errors->has('latitude'))
            <span class="help-block">{{ $errors->first('latitude') }}</span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('longitude') ? 'has-error' : '' }}">
        <label for="longitude" class="control-label">Longitude</label>
        <input type="text" class="form-control" name="longitude" placeholder="Longitude" value="{{ $foodplace->longitude }}">
        @if ($errors->has('longitude'))
            <span class="help-block">{{ $errors->first('longitude') }}</span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
        <label for="description" class="control-label">Description</label>
        <textarea name="description" cols="30" rows="5" class="form-control">{{ $foodplace->description }}</textarea>
        @if ($errors->has('description'))
            <span class="help-block">{{ $errors->first('description') }}</span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('menu_id') ? 'has-error' : '' }}">
        <label for="menu_id" class="control-label">Menu ID</label>
        <input type="text" class="form-control" name="menu_id" placeholder="Menu ID" value="{{ $foodplace->menu_id }}">
        @if ($errors->has('menu_id'))
            <span class="help-block">{{ $errors->first('menu_id') }}</span>
        @endif
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-info">Save</button>
        <a href="{{ route('foodplace.index') }}" class="btn btn-default">Back</a>
    </div>
</form>
@endsection