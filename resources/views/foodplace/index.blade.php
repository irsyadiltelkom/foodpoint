@extends('layouts.master')

@section('title','Food Places')

@section('content')
    <a href="{{ route('foodplace.create') }}" class="btn btn-info btn-sm">New Food Place</a>
    
    @if ($message = Session::get('message'))
        <div class="alert alert-success martop-sm">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-responsive martop-sm">
        <thead>
            <th>ID</th>
            <th>Name</th>
            <th>Action</th>
        </thead>
        <tbody>
            @foreach ($foodplaces as $place)
                <tr>
                    <td>{{ $place->id }}</td>
                    <td><a href="{{ route('foodplace.show', $place->id) }}">{{ $place->name }}</a></td>
                    <td>
                        <form action="{{ route('foodplace.destroy', $place->id) }}" method="post">
                            {{csrf_field()}}
                            {{ method_field('DELETE') }}
                            <a href="{{ route('foodplace.edit', $place->id) }}" class="btn btn-info btn-sm">Edit</a>
                            <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection