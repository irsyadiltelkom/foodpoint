@extends('layouts.master')

@section('title','Show Food Place')

@section('content')
<h4>{{ $foodplace->name }}</h4>
<p>{{ $foodplace->address }}</p>
<p>{{ $foodplace->latitude }}</p>
<p>{{ $foodplace->longitude }}</p>
<p>{{ $foodplace->description }}</p>
<p>{{ $foodplace->menu_id }}</p>
<a href="{{ route('foodplace.index') }}" class="btn btn-default">Back</a>
@endsection