<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FoodPlace extends Model
{
    protected $fillable = [
    	'name', 'address', 'latitude', 'longitude', 'description', 'menu_id'
    ];
    protected $dates = ['deleted_at'];
}
