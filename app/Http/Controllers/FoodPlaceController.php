<?php

namespace App\Http\Controllers;

use App\FoodPlace;
use Illuminate\Http\Request;

class FoodPlaceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $foodplaces = FoodPlace::latest()->get();
        return view('foodplace.index', compact('foodplaces'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('foodplace.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'address' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'description' => 'required',
            'menu_id' => 'required'
        ]);

        $foodplace = FoodPlace::create($request->all());

        return redirect()->route('foodplace.index')->with('message', 'Successfully Added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FoodPlace  $foodPlace
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $foodplace = FoodPlace::findOrFail($id);
        return view('foodplace.show', compact('foodplace'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FoodPlace  $foodPlace
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $foodplace = FoodPlace::findOrFail($id);
        return view('foodplace.edit', compact('foodplace'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FoodPlace  $foodPlace
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'address' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'description' => 'required',
            'menu_id' => 'required'
        ]);

        $foodplace = FoodPlace::findOrFail($id)->update($request->all());

        return redirect()->route('foodplace.index')->with('message', 'Successfully Edited!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FoodPlace  $foodPlace
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $foodplace = FoodPlace::findOrFail($id)->delete();
        return redirect()->route('foodplace.index')->with('message', 'Successfully Deleted!');
    }
}
